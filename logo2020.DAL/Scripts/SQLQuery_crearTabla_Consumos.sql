
/********************************************************/
/* Creamos la tabla para las ventas con nombre Consumos */
USE PFC_2020
GO
 
SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO
 
CREATE TABLE Consumos
(
id [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
VentaTotal int NOT NULL,
VentaTotalGrifo1 int NOT NULL,
VentaTotalGrifo2 int NOT NULL,
VentaTotalGrifo3 int NOT NULL,
VentaParcial int NOT NULL,
VentaParcialGrifo1 int NOT NULL,
VentaParcialGrifo2 int NOT NULL,
VentaParcialGrifo3 int NOT NULL,
fecha int NOT NULL
)

GO
/********************************************************/