
/************************************************************/
/* Creamos la tabla para notificaciones vía email */
USE PFC_2020
GO
 
SET ANSI_NULLS ON
GO
 
SET QUOTED_IDENTIFIER ON
GO
 
CREATE TABLE Usuarios
(
	id [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	nombre [varchar](120) NOT NULL,
	email [varchar](240) NOT NULL,
)

GO
/************************************************************/



