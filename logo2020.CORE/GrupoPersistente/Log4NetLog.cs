﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logo2020.Core
{

    /// <summary>
    /// Clase para registrar loggs
    /// </summary>
    public class Log4NetLog
    {
        /// <summary>
        /// Identificador del registro
        /// </summary>
        [Key] /// clave primaria - PK:PRIMARY KEY
        public int Id { get; set; }

        /// <summary>
        /// Fecha del registro
        /// </summary>
        [Required]
        [Display(Name = "Fecha")]
        public DateTime Date { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string Thread { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string Level { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string Logger { get; set; }

        /// <summary>
        /// Mensaje del error
        /// </summary>
        [Required]
        [Display(Name = "Mensaje")]
        public string Message { get; set; }

        /// <summary>
        /// Texto devuelto por la excepción
        /// </summary>
        [Required]
        [Display(Name = "Excepción")]
        public string Exception { get; set; }
    }
}
