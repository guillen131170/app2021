﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logo2020.CORE
{
    /// <summary>
    /// ESTA CLASE SERÁN LOS DATOS DE CONSUMOS
    /// QUE SE GUARDARÁN EN BASE DE DATOS
    /// </summary>   
    public class Consumos
    {
        /*################################################################*/
        #region atributos     
        /// <summary>
        /// identificador de dato de consumo
        /// </summary>
        private int id;

        /// <summary>
        /// consumo VentaTotal total (decilitros)
        /// </summary>
        private UInt32 ventaTotal;

        /// <summary>
        /// consumo VentaTotal linea 1 (decilitros)
        /// </summary>
        private UInt32 ventaTotalGrifo1;

        /// <summary>
        /// consumo VentaTotal linea 2 (decilitros)
        /// </summary>
        private UInt32 ventaTotalGrifo2;

        /// <summary>
        /// consumo VentaTotal linea 3 (decilitros)
        /// </summary>
        private UInt32 ventaTotalGrifo3;

        /// <summary>
        /// consumo de ese día (decilitros)
        /// </summary>
        private UInt32 ventaParcial;

        /// <summary>
        /// consumo de ese día linea 1 (decilitros)
        /// </summary>
        private UInt32 ventaParcialGrifo1;

        /// <summary>
        /// consumo de ese día linea 2 (decilitros)
        /// </summary>
        private UInt32 ventaParcialGrifo2;

        /// <summary>
        /// consumo de ese día linea 3 (decilitros)
        /// </summary>
        private UInt32 ventaParcialGrifo3;

        /// <summary>
        /// fecha completa del dato de consumo
        /// año + mes + día
        /// </summary>
        private int fecha;
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region /* constructores */

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public Consumos() 
        {
            VentaTotal = 0;
            VentaTotalGrifo1 = 0;
            VentaTotalGrifo2 = 0;
            VentaTotalGrifo3 = 0;
            VentaParcial = 0;
            VentaParcialGrifo1 = 0;
            VentaParcialGrifo2 = 0;
            VentaParcialGrifo3 = 0;
        }

        /// <summary>
        /// Constructor por parámetros
        /// </summary>
        public Consumos(int _fecha)
        {
            Fecha = _fecha;
        }

        /// <summary>
        /// Constructor copia
        /// </summary>
        public Consumos(Consumos o)
        {
            Id = o.Id;
            this.VentaTotal = o.VentaTotal;
            this.VentaTotalGrifo1 = o.VentaTotalGrifo1;
            this.VentaTotalGrifo2 = o.VentaTotalGrifo2;
            this.VentaTotalGrifo3 = o.VentaTotalGrifo3;
            this.VentaParcial = o.VentaParcial;
            this.VentaParcialGrifo1 = o.VentaParcialGrifo1;
            this.VentaParcialGrifo2 = o.VentaParcialGrifo2;
            this.VentaParcialGrifo3 = o.VentaParcialGrifo3;
            Fecha = o.Fecha;
        }
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region propiedades
        [Required]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [Required]
        public UInt32 VentaTotal
        {
            get { return ventaTotal; }
            set { ventaTotal = value; }
        }

        [Required]
        public UInt32 VentaTotalGrifo1
        {
            get { return ventaTotalGrifo1; }
            set { ventaTotalGrifo1 = value; }
        }

        [Required]
        public UInt32 VentaTotalGrifo2
        {
            get { return ventaTotalGrifo2; }
            set { ventaTotalGrifo2 = value; }
        }

        [Required]
        public UInt32 VentaTotalGrifo3
        {
            get { return ventaTotalGrifo3; }
            set { ventaTotalGrifo3 = value; }
        }

        [Required]
        public UInt32 VentaParcial
        {
            get { return ventaParcial; }
            set { ventaParcial = value; }
        }

        [Required]
        public UInt32 VentaParcialGrifo1
        {
            get { return ventaParcialGrifo1; }
            set { ventaParcialGrifo1 = value; }
        }

        [Required]
        public UInt32 VentaParcialGrifo2
        {
            get { return ventaParcialGrifo2; }
            set { ventaParcialGrifo2 = value; }
        }

        [Required]
        public UInt32 VentaParcialGrifo3
        {
            get { return ventaParcialGrifo3; }
            set { ventaParcialGrifo3 = value; }
        }

        [Required]
        public int Fecha
        {
            get { return fecha; }
            set { fecha = value; }
        }
        #endregion
        /*################################################################*/
    }
}
