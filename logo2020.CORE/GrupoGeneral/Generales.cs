﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logo2020.CORE
{
    public class Generales
    {
        #region CONVIERTE UN ENTERO EN 4 BYTES
        public byte[] IntToArrayByte4(int value)
        {
            byte[] buffer = new byte[4];
            buffer[0] = (byte)((value >> 24) & 0xFF);
            buffer[1] = (byte)((value >> 16) & 0xFF);
            buffer[2] = (byte)((value >> 8) & 0xFF);
            buffer[3] = (byte)((value) & 0xFF);
            return buffer;
        }
        #endregion

        #region CONVIERTE UN ENTERO EN 2 BYTES
        public byte[] IntToArrayByte2(int value)
        {
            byte[] buffer = new byte[2];
            buffer[2] = (byte)((value >> 8) & 0xFF);
            buffer[3] = (byte)((value) & 0xFF);
            return buffer;
        }
        #endregion
    }
}
