﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logo2020.CORE
{
    public static class Constantes
    {
        /*
         * 
        public struct N_BARRILES_LINEA1_BYTE_1  {
            public int direccion;
            public int valor;
            public N_BARRILES_LINEA1_BYTE_1 (int d, int v) { direccion = d; valor = v; }
        }
        */
        #region CONSTANTES DEL CORREO ELECTRÓNICO
        public const string ADDRESS_MAIL_FROM = "intercambiadordebarriles@hotmail.com";
        public const string PASSWORD_MAIL_FROM = "Virtuino";
        public const int MAX_NTRY_SENDMAIL = 5;
        #endregion


        ///////////////////////////////////////////////////////////////////////
        //#####################################################################
        #region CONTADOR DE LITROS DEl BARRIL ACTIVO DE CADA LINEA 1, 2 Y 3
        #region LINEA 1 - 4 BYTES - DIRECCIONES DE MEMORIA: DE 0 A 3, DE 4 A 7, DE 8 A 11
        #region BARRIL 1
        public const int N_LITROS_BARRIL1_LINEA1_BYTE_0 = 0;
        public const int N_LITROS_BARRIL1_LINEA1_BYTE_1 = 1;
        public const int N_LITROS_BARRIL1_LINEA1_BYTE_2 = 2;
        public const int N_LITROS_BARRIL1_LINEA1_BYTE_3 = 3;
        #endregion
        #region BARRIL 2
        public const int N_LITROS_BARRIL2_LINEA1_BYTE_0 = 4;
        public const int N_LITROS_BARRIL2_LINEA1_BYTE_1 = 5;
        public const int N_LITROS_BARRIL2_LINEA1_BYTE_2 = 6;
        public const int N_LITROS_BARRIL2_LINEA1_BYTE_3 = 7;
        #endregion
        #region BARRIL 3
        public const int N_LITROS_BARRIL3_LINEA1_BYTE_0 = 8;
        public const int N_LITROS_BARRIL3_LINEA1_BYTE_1 = 9;
        public const int N_LITROS_BARRIL3_LINEA1_BYTE_2 = 10;
        public const int N_LITROS_BARRIL3_LINEA1_BYTE_3 = 11;
        #endregion
        #endregion
        #region LINEA 2 - 4 BYTES - DIRECCIONES DE MEMORIA: 28 - 31, 32 - 35, 36 - 39
        #region BARRIL 1
        public const int N_LITROS_BARRIL1_LINEA2_BYTE_0 = 28;
        public const int N_LITROS_BARRIL1_LINEA2_BYTE_1 = 29;
        public const int N_LITROS_BARRIL1_LINEA2_BYTE_2 = 30;
        public const int N_LITROS_BARRIL1_LINEA2_BYTE_3 = 31;
        #endregion
        #region BARRIL 2
        public const int N_LITROS_BARRIL2_LINEA2_BYTE_0 = 32;
        public const int N_LITROS_BARRIL2_LINEA2_BYTE_1 = 33;
        public const int N_LITROS_BARRIL2_LINEA2_BYTE_2 = 34;
        public const int N_LITROS_BARRIL2_LINEA2_BYTE_3 = 35;
        #endregion
        #region BARRIL 3
        public const int N_LITROS_BARRIL3_LINEA2_BYTE_0 = 36;
        public const int N_LITROS_BARRIL3_LINEA2_BYTE_1 = 37;
        public const int N_LITROS_BARRIL3_LINEA2_BYTE_2 = 38;
        public const int N_LITROS_BARRIL3_LINEA2_BYTE_3 = 39;
        #endregion
        #endregion
        #region LINEA 3 - 4 BYTES - DIRECCIONES DE MEMORIA: 56 - 59, 60 - 63, 64 - 67
        #region BARRIL 1
        public const int N_LITROS_BARRIL1_LINEA3_BYTE_0 = 56;
        public const int N_LITROS_BARRIL1_LINEA3_BYTE_1 = 57;
        public const int N_LITROS_BARRIL1_LINEA3_BYTE_2 = 58;
        public const int N_LITROS_BARRIL1_LINEA3_BYTE_3 = 59;
        #endregion
        #region BARRIL 2
        public const int N_LITROS_BARRIL2_LINEA3_BYTE_0 = 60;
        public const int N_LITROS_BARRIL2_LINEA3_BYTE_1 = 61;
        public const int N_LITROS_BARRIL2_LINEA3_BYTE_2 = 62;
        public const int N_LITROS_BARRIL2_LINEA3_BYTE_3 = 63;
        #endregion
        #region BARRIL 3
        public const int N_LITROS_BARRIL3_LINEA3_BYTE_0 = 64;
        public const int N_LITROS_BARRIL3_LINEA3_BYTE_1 = 65;
        public const int N_LITROS_BARRIL3_LINEA3_BYTE_2 = 66;
        public const int N_LITROS_BARRIL3_LINEA3_BYTE_3 = 67;
        #endregion
        #endregion
        #endregion
        //#####################################################################
        ///////////////////////////////////////////////////////////////////////


        ///////////////////////////////////////////////////////////////////////
        //#####################################################################
        #region CONTADOR DE BARRILES DE CADA LÍNEA: 1, 2 Y 3
        #region LINEA 1 - 4 BYTES - DIRECCIONES DE MEMORIA: 118 - 119 - 120 - 121
        public const int N_BARRILES_LINEA1_BYTE_1 = 118;
        public const int N_BARRILES_LINEA1_BYTE_2 = 119;
        public const int N_BARRILES_LINEA1_BYTE_3 = 120;
        public const int N_BARRILES_LINEA1_BYTE_4 = 121;
        #endregion
        #region LINEA 2 - 4 BYTES - DIRECCIONES DE MEMORIA: 122 - 123 - 124 - 125
        public const int N_BARRILES_LINEA2_BYTE_1 = 122;
        public const int N_BARRILES_LINEA2_BYTE_2 = 123;
        public const int N_BARRILES_LINEA2_BYTE_3 = 124;
        public const int N_BARRILES_LINEA2_BYTE_4 = 125;
        #endregion
        #region LINEA 3 - 4 BYTES - DIRECCIONES DE MEMORIA: 126 - 127 - 128 - 129
        public const int N_BARRILES_LINEA3_BYTE_1 = 126;
        public const int N_BARRILES_LINEA3_BYTE_2 = 127;
        public const int N_BARRILES_LINEA3_BYTE_3 = 128;
        public const int N_BARRILES_LINEA3_BYTE_4 = 129;
        #endregion
        #endregion
        //#####################################################################
        ///////////////////////////////////////////////////////////////////////
        

        ///////////////////////////////////////////////////////////////////////
        //#####################################################################
        #region ELECTROVALVULAS DE BARRILES (1-2-3 / 11-12-13 / 21-22-23) - BARRILES ACTIVOS
        #region LINEA 1 - 3 BYTES CORRESPONDIENTES A LOS 3 BARRILES - DIRECCIONES DE MEMORIA: 214 - 215 - 216
        /// <summary>
        /// ELECTROVALVULAS DE BARRILES (1-2-3 / 11-12-13 / 21-22-23) - BARRILES ACTIVOS
        /// 3 BYTES POR LINEA (BA1 BYTE 0 - BA2 BYTE 1 - BA3 BYTE 2)
        /// 1 ACTIVO / 0 INACTIVO
        /// </summary>
        public const int DIRECCION_MEMORIA_EVA1_LINEA1_BARRIL1 = 214;
        public const int DIRECCION_MEMORIA_EVA2_LINEA1_BARRIL2 = 215;
        public const int DIRECCION_MEMORIA_EVA3_LINEA1_BARRIL3 = 216;
        #endregion
        #region LINEA 2 - 3 BYTES CORRESPONDIENTES A LOS 3 BARRILES - DIRECCIONES DE MEMORIA: 224 - 225 - 226
        /// <summary>
        /// BARRILES ACTIVOS
        /// 3 BYTES POR LINEA (BA1 BYTE 0 - BA2 BYTE 1 - BA3 BYTE 2)
        /// 1 ACTIVO / 0 INACTIVO
        /// </summary>
        public const int DIRECCION_MEMORIA_EVA11_LINEA2_BARRIL1 = 224;
        public const int DIRECCION_MEMORIA_EVA12_LINEA2_BARRIL2 = 225;
        public const int DIRECCION_MEMORIA_EVA13_LINEA2_BARRIL3 = 226;
        #endregion
        #region LINEA 3 - 3 BYTES CORRESPONDIENTES A LOS 3 BARRILES - DIRECCIONES DE MEMORIA: 234 - 235 - 236
        /// <summary>
        /// BARRILES ACTIVOS
        /// 3 BYTES POR LINEA (BA1 BYTE 0 - BA2 BYTE 1 - BA3 BYTE 2)
        /// 1 ACTIVO / 0 INACTIVO
        /// </summary>
        public const int DIRECCION_MEMORIA_EVA21_LINEA3_BARRIL1 = 234;
        public const int DIRECCION_MEMORIA_EVA22_LINEA3_BARRIL2 = 235;
        public const int DIRECCION_MEMORIA_EVA23_LINEA3_BARRIL3 = 236;
        #endregion
        #endregion
        //#####################################################################
        ///////////////////////////////////////////////////////////////////////


        ///////////////////////////////////////////////////////////////////////
        //#####################################################################
        #region DIRECCIONES DE MEMORIA PARA COMPROBAR BARRILES LLENOS Y VACÍOS (211 - 212 - 213)
        #region LINEA 1 - 3 BYTES CORRESPONDIENTES A LOS 3 BARRILES - DIRECCIONES DE MEMORIA: 211 - 212 - 213
        /// <summary>
        /// 1 BYTE POR BARRIL - 3 BYTES CORRESPONDIENTES A LOS 3 BARRILES DE LINEA 1
        /// INDICACION: 0 PARA BARRIL LLENO / OTRO VALOR PARA BARRIL VACIO
        /// </summary>
        public const int DIRECION_MEMORIA_BARRIL1_LLENO_LINEA1 = 211;
        public const int DIRECION_MEMORIA_BARRIL2_LLENO_LINEA1 = 212;
        public const int DIRECION_MEMORIA_BARRIL3_LLENO_LINEA1 = 213;
        #endregion
        #region LINEA 2 - 3 BYTES CORRESPONDIENTES A LOS 3 BARRILES - DIRECCIONES DE MEMORIA: 221 - 222 - 223
        /// <summary>
        /// 1 BYTE POR BARRIL - 3 BYTES CORRESPONDIENTES A LOS 3 BARRILES DE LINEA 2
        /// INDICACION: 0 PARA BARRIL LLENO / OTRO VALOR PARA BARRIL VACIO
        /// </summary>
        public const int DIRECION_MEMORIA_BARRIL1_LLENO_LINEA2 = 221;
        public const int DIRECION_MEMORIA_BARRIL2_LLENO_LINEA2 = 222;
        public const int DIRECION_MEMORIA_BARRIL3_LLENO_LINEA2 = 223;
        #endregion
        #region LINEA 3 - 3 BYTES CORRESPONDIENTES A LOS 3 BARRILES - DIRECCIONES DE MEMORIA: 231 - 232 - 233
        /// <summary>
        /// 1 BYTE POR BARRIL - 3 BYTES CORRESPONDIENTES A LOS 3 BARRILES DE LINEA 3
        /// INDICACION: 0 PARA BARRIL LLENO / OTRO VALOR PARA BARRIL VACIO
        /// </summary>
        public const int DIRECION_MEMORIA_BARRIL1_LLENO_LINEA3 = 231;
        public const int DIRECION_MEMORIA_BARRIL2_LLENO_LINEA3 = 232;
        public const int DIRECION_MEMORIA_BARRIL3_LLENO_LINEA3 = 233;
        #endregion
        #endregion
        //#####################################################################
        ///////////////////////////////////////////////////////////////////////


        ///////////////////////////////////////////////////////////////////////
        //#####################################################################
        #region DIRECCIONES DE MEMORIA PARA AVANCE DE BARRIL (311 - 321 - 331)
        #region LINEA 1, 2 Y 3 - 1 BYTE POR LINEA DE PRODUCTO/GRIFO - DIRECCIONES DE MEMORIA: 311 - 321 - 331
        /// <summary>
        /// DIRECCIONES 311 - 321 - 331
        /// 1 BYTE POR LINEA DE PRODUCTO/GRIFO
        /// PARA AVANZAR BARRIL: ESCRIBIR 1 Y DESPUES 0
        /// </summary>
        public const int DIRECCION_MEMORIA_AVANCE311_LINEA1 = 311;
        public const int DIRECCION_MEMORIA_AVANCE321_LINEA2 = 321;
        public const int DIRECCION_MEMORIA_AVANCE331_LINEA3 = 331;
        #endregion
        #endregion
        //#####################################################################
        ///////////////////////////////////////////////////////////////////////


        ///////////////////////////////////////////////////////////////////////
        //#####################################################################
        #region DIRECCIONES DE MEMORIA PARA REPOSICION DE BARRILES (313 - 323 - 333)
        #region LINEA 1, 2 Y 3 - 1 BYTE POR LINEA DE PRODUCTO/GRIFO - DIRECCIONES DE MEMORIA: 313 - 323 - 333
        /// <summary>
        /// DIRECCIONES 313 - 323 - 333
        /// 1 BYTE POR LINEA DE PRODUCTO/GRIFO
        /// PAARA REPOSICION DE BARRILES: ESCRIBIR 1 Y DESPUES 0
        /// </summary>
        public const int DIRECCION_MEMORIA_REPOSICION313_LINEA1 = 313;
        public const int DIRECCION_MEMORIA_REPOSICION323_LINEA2 = 323;
        public const int DIRECCION_MEMORIA_REPOSICION333_LINEA3 = 333;
        #endregion
        #endregion
        //#####################################################################
        ///////////////////////////////////////////////////////////////////////


        ///////////////////////////////////////////////////////////////////////
        //#####################################################################
        #region ELECTROVALVULAS DE GRIFO ABIERTO/CERRADO (5 - 15 - 25)
        #region LINEA 1, 2 Y 3 - 1 BYTE POR GRIFO/ELECTROVALVULA - DIRECCIONES DE MEMORIA: 310 - 320 - 330
        /// <summary>
        /// ELECTROVALVULAS 5, 15 Y 25
        /// 1 BYTE POR ELECTROVALVULA - DIRECCIONES DE MEMORIA: 310 - 320 - 330
        /// 0 ABIERTA / OTRO VALOR CERRADA 
        /// </summary>
        public const int DIRECCION_MEMORIA_EVA5_LINEA1_GRIFO = 310;
        public const int DIRECCION_MEMORIA_EVA15_LINEA2_GRIFO = 320;
        public const int DIRECCION_MEMORIA_EVA25_LINEA3_GRIFO = 330;
        #endregion
        #endregion
        //#####################################################################
        ///////////////////////////////////////////////////////////////////////


        ///////////////////////////////////////////////////////////////////////
        //#####################################################################
        #region ELECTROVALVULAS DE DETECTOR UP/DOWN (4 - 14 - 24)
        #region LINEA 1, 2 Y 3 - 1 BYTE POR DETECTOR/ELECTROVALVULA - DIRECCIONES DE MEMORIA: 210 - 220 - 230
        /// <summary>
        /// ELECTROVALVULAS DE DETECTOR/FLOTADOR UP/DOWN (4 - 14 - 24)
        /// 1 BYTE POR DETECTOR - DIRECCIONES DE MEMORIA: 210 - 220 - 230
        /// 0 FLOTADOR UP / OTRO VALOR FLOTADOR DOWN
        /// </summary>
        public const int DIRECCION_MEMORIA_EVA4_LINEA1_DETECTOR = 210;
        public const int DIRECCION_MEMORIA_EVA14_LINEA2_DETECTOR = 220;
        public const int DIRECCION_MEMORIA_EVA24_LINEA3_DETECTOR = 230;
        #endregion
        #endregion
        //#####################################################################
        ///////////////////////////////////////////////////////////////////////


        ///////////////////////////////////////////////////////////////////////
        //#####################################################################
        #region ALARMAS - 1 BYTE POR ALARMA

        #region ALARMA_CO2: 208
        /// <summary>
        /// DIRECCION DE MEMORIA ALARMA_CO2: 208
        /// </summary>
        public const int DIRECCION_MEMORIA_ALARMA_CO2 = 208;
        /// <summary>
        /// VALOR EN LA DIRECCION DE MEMORIA ALARMA_CO2
        /// 1 PARA ALARMA OK / OTRO VALOR PARA ESTADO ERROR
        /// </summary>
        public const int VALOR_OK_ALARMA_CO2 = 1;
        #endregion

        #region ALARMA_COMPRESOR_AIRE: 255
        /// <summary>
        /// DIRECCION DE MEMORIA ALARMA_COMPRESOR_AIRE: 255
        /// </summary>
        public const int DIRECCION_MEMORIA_ALARMA_COMPRESOR_AIRE = 255;
        /// <summary>
        /// VALOR EN LA DIRECCION DE MEMORIA ALARMA_COMPRESOR_AIRE
        /// 1 PARA ALARMA OK / OTRO VALOR PARA ESTADO ERROR
        /// </summary>
        public const int VALOR_OK_ALARMA_COMPRESOR_AIRE = 1;
        #endregion

        #region ALARMA_PUERTA_ABIERTA: 209
        /// <summary>
        /// DIRECCION DE MEMORIA ALARMA_PUERTA_ABIERTA: 209
        /// </summary>
        public const int DIRECCION_MEMORIA_ALARMA_PUERTA_ABIERTA = 209;
        /// <summary>
        /// VALOR EN LA DIRECCION DE MEMORIA ALARMA_PUERTA_ABIERTA
        /// 1 PARA ALARMA OK / OTRO VALOR PARA ESTADO ERROR
        /// </summary>
        public const int VALOR_OK_ALARMA_PUERTA_ABIERTA = 1;
        #endregion

        #region ALARMA_FUSIBLE: 254 - ESTA ALARMA ES PROVISIONAL PARA REALIZAR PRUEBAS
        /// <summary>
        /// DIRECCION DE MEMORIA ALARMA_FUSIBLE: 254
        /// </summary>
        public const int DIRECCION_MEMORIA_ALARMA_FUSIBLE = 254;
        /// <summary>
        /// VALOR EN LA DIRECCION DE MEMORIA ALARMA_FUSIBLE
        /// ¿? PARA ALARMA OK / OTRO VALOR PARA ESTADO ERROR
        /// </summary>
        public const int VALOR_OK_ALARMA_FUSIBLE = 1;
        #endregion

        #region ALARMA_FUSIBLE: 250 - DESCOMENTAR PARA PONER VALORES CORRECTOS
        /*
        /// <summary>
        /// DIRECCION DE MEMORIA ALARMA_FUSIBLE: 250
        /// </summary>
        public const int DIRECCION_MEMORIA_ALARMA_FUSIBLE = 250;
        /// <summary>
        /// VALOR EN LA DIRECCION DE MEMORIA ALARMA_FUSIBLE
        /// ¿? PARA ALARMA OK / OTRO VALOR PARA ESTADO ERROR
        /// </summary>
        public const int VALOR_OK_ALARMA_FUSIBLE = 1;
        */
        #endregion

        #region PARA REALIZAR PRUEBAS - ALARMA_FICTICIA: 511
        /// <summary>
        /// DIRECCION DE MEMORIA ALARMA_FICTICIA: 511
        /// </summary>
        public const int DIRECCION_MEMORIA_ALARMA_FICTICIA = 511;
        /// <summary>
        /// VALOR EN LA DIRECCION DE MEMORIA ALARMA_FICTICIA
        /// 1 PARA ALARMA OK / OTRO VALOR PARA ESTADO ERROR
        /// </summary>
        public const int VALOR_OK_ALARMA_FICTICIA = 1;
        #endregion
        #endregion
        //#####################################################################
        ///////////////////////////////////////////////////////////////////////
    }


    ///////////////////////////////////////////////////////////////////////
    //#####################################################################
    /*
    #region BORRAR - OBSOLETO
    /// <summary>
    /// Estructura de todas las alarmas agrupadas
    /// </summary>
    public struct Alarmas
    {
        public Alarmas(Alarma co2, Alarma puerta, Alarma compresor, Alarma fusible)
        {
            Co2 = co2;
            Puerta = puerta;
            Compresor = compresor;
            Fusible = fusible;
        }

        public Alarma Co2 { get; }
        public Alarma Puerta { get; }
        public Alarma Compresor { get; }
        public Alarma Fusible { get; }
    }

    /// <summary>
    /// Estructura de una alarma
    /// Cada alarma tiene: direccion de memoria y valor
    /// </summary>
    public struct Alarma
    {
        public Alarma(int direccion, byte valor)
        {
            Direccion = direccion;
            Valor = valor;
        }

        public int Direccion { get; }
        public byte Valor { get; }
    } 
    #endregion
    */
    //#####################################################################
    ///////////////////////////////////////////////////////////////////////
}
