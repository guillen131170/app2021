﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logo2020.CORE.GrupoComponentes
{
    /// <summary>
    /// Esta clase representa una estructura con todas las alarmas del sistema
    /// </summary>
    public class Alarmas
    {
        #region Propiedades
        private Componentes co2;
        private Componentes puerta;
        private Componentes compresor;
        private Componentes fusible;

        #region VARIABLES AUXILIARES UTILIZADAS PARA NO REPETIR ENVIO DE CORREO
        /// <summary>
        /// valor 0 indica que no se ha enviado
        /// otro valor indica que ya se ha enviado un correo
        /// </summary>
        private byte auxiliar_co2;
        private byte auxiliar_puerta;
        private byte auxiliar_compresor;
        private byte auxiliar_fusible; 
        #endregion
        #endregion


        #region Constructores
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public Alarmas() 
        {
            Co2 = new Componentes();
            Co2.Direccion = Constantes.DIRECCION_MEMORIA_ALARMA_CO2;
            Co2.Valor = 255;
            Puerta = new Componentes();
            Puerta.Direccion = Constantes.DIRECCION_MEMORIA_ALARMA_PUERTA_ABIERTA;
            Puerta.Valor = 255;
            Compresor = new Componentes();
            Compresor.Direccion = Constantes.DIRECCION_MEMORIA_ALARMA_COMPRESOR_AIRE;
            Compresor.Valor = 255;
            Fusible = new Componentes();
            Fusible.Direccion = Constantes.DIRECCION_MEMORIA_ALARMA_FUSIBLE;
            Fusible.Valor = 255;

            Auxiliar_co2 = 0;
            Auxiliar_puerta = 0;
            Auxiliar_compresor = 0;
            Auxiliar_fusible = 0;
        }
        #endregion


        #region Propiedades
        public Componentes Co2 { get; set; }
        public Componentes Puerta { get; set; }
        public Componentes Compresor { get; set; }
        public Componentes Fusible { get; set; }

        public byte Auxiliar_co2 { get; set; }
        public byte Auxiliar_puerta { get; set; }
        public byte Auxiliar_compresor { get; set; }
        public byte Auxiliar_fusible { get; set; }
        #endregion
    }


    /// <summary>
    /// Clase que representa el estado de las alarmas - activadas/desactivadas
    /// </summary>
    public class EstadoAlarmas
    {
        #region Constructores
        public EstadoAlarmas() 
        {
            Co2 = false;
            Puerta = false;
            Compresor = false;
            Fusible = false;
        } 
        #endregion


        #region Propiedades
        public bool Co2 { get; set; }
        public bool Puerta { get; set; }
        public bool Compresor { get; set; }
        public bool Fusible { get; set; } 
        #endregion
    }
}
