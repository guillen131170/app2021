﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logo2020.CORE
{
    public class Ventas
    {
        #region Atributos
        #region VALORES INICIALES - BARRIL ACTIVO/NUMERO LTS DEL BARRIL ACTIVO/NUMERO BARRILES DE LA LINEA
        /// <summary>
        /// Indica los barriles activos inicialmente
        /// </summary>
        public int[] barrilActivoInicial = new int[3];
        /// <summary>
        /// Indica el número inicial de litros que quedan por cada barril activo
        /// </summary>
        public UInt32[] numLitrosBarrilActivoInicial = new UInt32[3];
        /// <summary>
        /// Indica el número inicial de barriles VentaParcials por cada linea
        /// </summary>
        public UInt32[] numBarrilesInicial = new UInt32[3];
        #endregion

        #region VALORES FINALES - BARRIL ACTIVO/NUMERO LTS DEL BARRIL ACTIVO/NUMERO BARRILES DE LA LINEA
        /// <summary>
        /// Indica los barriles activos actualmente
        /// </summary>
        public int[] barrilActivoActual = new int[3];
        /// <summary>
        /// Indica el número actual de litros que quedan por cada barril activo
        /// </summary>
        public UInt32[] numLitrosBarrilActivoActual = new UInt32[3];
        /// <summary>
        /// Indica el número actual de barriles VentaParcials por cada linea
        /// </summary>
        public UInt32[] numBarrilesActual = new UInt32[3];
        #endregion

        #region REPRESENTA LOS LITROS QUE TIENEN LOS 9 BARRILES
        /// <summary>
        /// Indica el número de litros de cada barril
        /// Barriles 1 2 y 3 de Lineas 1 2 y 3
        /// Total 9 barriles
        /// </summary>
        public UInt32[,] numLitros = new UInt32[3, 3];
        #endregion

        #region CONTIENE LOS CONSUMOS PARA GUARDADO DE BBDD
        private Consumos consumo; 
        #endregion
        #endregion


        #region CONSTRUCTORES
        public Ventas()
        {
            consumo = new Consumos();
        }
        #endregion


        #region PROPIEDADES
        public Consumos Consumo { get; set; }
        #endregion
    }
}
