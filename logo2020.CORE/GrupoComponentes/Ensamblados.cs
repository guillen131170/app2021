﻿using logo2020.CORE.GrupoComponentes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace logo2020.CORE
{
    public class Ensamblados
    {
        #region Atributos
        /// <summary>
        /// Cada módulo representa un módulo o línea de producto,
        /// los TRES módulos o líneas de producto forman un ENSAMBLADO COMPLETO
        /// </summary>
        #region MODULOS A - B - C
        private ModulosTipoA moduloA; //Linea de producto 1
        private ModulosTipoB moduloB; //Linea de producto 2
        private ModulosTipoC moduloC; //Linea de producto 3 
        #endregion
        /// <summary>
        /// Array que representa las celdas de memoria de Logo
        /// </summary>
        private const int NUMERO_BYTES = 512;
        public byte[] bufferMemoria = new byte[NUMERO_BYTES];
        /// <summary>
        /// Clase que representa una estructura con todas las alarmas del sistema
        /// </summary>
        private Alarmas alarma;
        #endregion


        #region Constructores
        /// <summary>
        /// Constructor por defecto y sin parámetros
        /// </summary>
        public Ensamblados() 
        {
            #region MODULO A - ELECTROVALVULAS: BARRIL 1 - 2 - 3 / DETECTOR 4 / GRIFO 5
            ModuloA = new ModulosTipoA();
            #endregion
            #region MODULO B - ELECTROVALVULAS: BARRIL 11 - 12 - 13 / DETECTOR 14 / GRIFO 15
            ModuloB = new ModulosTipoB();
            #endregion
            #region MODULO B - ELECTROVALVULAS: BARRIL 21 - 22 - 23 / DETECTOR 24 / GRIFO 25
            ModuloC = new ModulosTipoC();
            #endregion

            #region ALARMAS DEL SISTEMA
            Alarma = new Alarmas(); 
            #endregion

            #region INICIA EL ARRAY DE MEMORIA CON VALORES 255
            for (int i = 0; i < NUMERO_BYTES; i++)
            {
                bufferMemoria[i] = 255;
            }
            #endregion
        }
        #endregion


        #region Métodos
        /// <summary>
        /// Actualiza celdas de memoria específicas de logo
        /// </summary>
        /// <param name="celdasLogo">los primeros 512 (NUMERO_BYTES) bytes de logo</param>
        public void ActualizaValores(byte[] celdasLogo)
        {
            #region MODULO A
            #region ELECTROVALVULAS: BARRIL 1 - 2 - 3 / DETECTOR 4 / GRIFO 5
            ModuloA.ElectrovalvulaBarril1.Valor = celdasLogo[Constantes.DIRECCION_MEMORIA_EVA1_LINEA1_BARRIL1];
            ModuloA.ElectrovalvulaBarril2.Valor = celdasLogo[Constantes.DIRECCION_MEMORIA_EVA2_LINEA1_BARRIL2];
            ModuloA.ElectrovalvulaBarril3.Valor = celdasLogo[Constantes.DIRECCION_MEMORIA_EVA3_LINEA1_BARRIL3];
            ModuloA.ElectrovalvulaDetector.Valor = celdasLogo[Constantes.DIRECCION_MEMORIA_EVA4_LINEA1_DETECTOR];
            ModuloA.ElectrovalvulaGrifo.Valor = celdasLogo[Constantes.DIRECCION_MEMORIA_EVA5_LINEA1_GRIFO];
            #endregion
            #region CONTENIDO DE BARRILES - LINEA 1 - 3 BARRILES (1,2 Y 3)
            ModuloA.Barril1.Valor = celdasLogo[Constantes.DIRECION_MEMORIA_BARRIL1_LLENO_LINEA1];
            ModuloA.Barril2.Valor = celdasLogo[Constantes.DIRECION_MEMORIA_BARRIL2_LLENO_LINEA1];
            ModuloA.Barril3.Valor = celdasLogo[Constantes.DIRECION_MEMORIA_BARRIL3_LLENO_LINEA1];
            #endregion
            #endregion

            #region MODULO B
            #region ELECTROVALVULAS: BARRIL 1 - 2 - 3 / DETECTOR 4 / GRIFO 5
            ModuloB.ElectrovalvulaBarril1.Valor = celdasLogo[Constantes.DIRECCION_MEMORIA_EVA11_LINEA2_BARRIL1];
            ModuloB.ElectrovalvulaBarril2.Valor = celdasLogo[Constantes.DIRECCION_MEMORIA_EVA12_LINEA2_BARRIL2];
            ModuloB.ElectrovalvulaBarril3.Valor = celdasLogo[Constantes.DIRECCION_MEMORIA_EVA13_LINEA2_BARRIL3];
            ModuloB.ElectrovalvulaDetector.Valor = celdasLogo[Constantes.DIRECCION_MEMORIA_EVA14_LINEA2_DETECTOR];
            ModuloB.ElectrovalvulaGrifo.Valor = celdasLogo[Constantes.DIRECCION_MEMORIA_EVA15_LINEA2_GRIFO];
            #endregion
            #region CONTENIDO DE BARRILES - LINEA 2 - 3 BARRILES (1,2 Y 3)
            ModuloB.Barril1.Valor = celdasLogo[Constantes.DIRECION_MEMORIA_BARRIL1_LLENO_LINEA2];
            ModuloB.Barril2.Valor = celdasLogo[Constantes.DIRECION_MEMORIA_BARRIL2_LLENO_LINEA2];
            ModuloB.Barril3.Valor = celdasLogo[Constantes.DIRECION_MEMORIA_BARRIL3_LLENO_LINEA2];
            #endregion
            #endregion

            #region MODULO C
            #region ELECTROVALVULAS: BARRIL 1 - 2 - 3 / DETECTOR 4 / GRIFO 5
            ModuloC.ElectrovalvulaBarril1.Valor = celdasLogo[Constantes.DIRECCION_MEMORIA_EVA21_LINEA3_BARRIL1];
            ModuloC.ElectrovalvulaBarril2.Valor = celdasLogo[Constantes.DIRECCION_MEMORIA_EVA22_LINEA3_BARRIL2];
            ModuloC.ElectrovalvulaBarril3.Valor = celdasLogo[Constantes.DIRECCION_MEMORIA_EVA23_LINEA3_BARRIL3];
            ModuloC.ElectrovalvulaDetector.Valor = celdasLogo[Constantes.DIRECCION_MEMORIA_EVA24_LINEA3_DETECTOR];
            ModuloC.ElectrovalvulaGrifo.Valor = celdasLogo[Constantes.DIRECCION_MEMORIA_EVA25_LINEA3_GRIFO];
            #endregion
            #region CONTENIDO DE BARRILES - LINEA 3 - 3 BARRILES (1,2 Y 3)
            ModuloC.Barril1.Valor = celdasLogo[Constantes.DIRECION_MEMORIA_BARRIL1_LLENO_LINEA3];
            ModuloC.Barril2.Valor = celdasLogo[Constantes.DIRECION_MEMORIA_BARRIL2_LLENO_LINEA3];
            ModuloC.Barril3.Valor = celdasLogo[Constantes.DIRECION_MEMORIA_BARRIL3_LLENO_LINEA3];
            #endregion
            #endregion

            #region ACTUALIZA LAS 4 ALARMAS DEL SISTEMA: CO2 - PUERTA - COMPRESOR - FUSIBLE
            Alarma.Co2.Valor = celdasLogo[Constantes.DIRECCION_MEMORIA_ALARMA_CO2];
            Alarma.Puerta.Valor = celdasLogo[Constantes.DIRECCION_MEMORIA_ALARMA_PUERTA_ABIERTA];
            Alarma.Compresor.Valor = celdasLogo[Constantes.DIRECCION_MEMORIA_ALARMA_COMPRESOR_AIRE];
            Alarma.Fusible.Valor = celdasLogo[Constantes.DIRECCION_MEMORIA_ALARMA_FUSIBLE];
            #endregion
        }

        /// <summary>
        /// Copia toda la memoria de logo
        /// </summary>
        /// <param name="celdasLogo">los primeros 512 (NUMERO_BYTES) bytes de logo</param>
        public void ActualizaMemoria(byte[] celdasLogo)
        {
            for (int i = 0; i < NUMERO_BYTES; i++)
            {
                bufferMemoria[i] = celdasLogo[i];
            }
        }
        #endregion


        #region Propiedades
        public ModulosTipoA ModuloA { get; set; }
        public ModulosTipoB ModuloB { get; set; }
        public ModulosTipoC ModuloC { get; set; }

        public Alarmas Alarma { get; set; }
        #endregion
    }
}
