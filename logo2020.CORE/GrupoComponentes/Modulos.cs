﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace logo2020.CORE
{
    #region CLASE GENERICA MODULOS
    /// <summary>
    /// Clase base del resto de tipos de módulos
    /// </summary>
    public class Modulos
    {
        #region Atributos
        #endregion


        #region Constructores
        public Modulos()
        {
            #region MODULO - ELECTROVALVULAS: BARRIL 1 - 2 - 3 / DETECTOR 4 / GRIFO 5
            ElectrovalvulaBarril1 = new Componentes();
            ElectrovalvulaBarril2 = new Componentes();
            ElectrovalvulaBarril3 = new Componentes();
            ElectrovalvulaDetector = new Componentes();
            ElectrovalvulaGrifo = new Componentes();
            #endregion

            #region CONTENIDO DE BARRILES - 1 LINEA SON 3 BARRILES (1,2 Y 3)
            Barril1 = new Componentes();
            Barril2 = new Componentes();
            Barril3 = new Componentes();
            #endregion
        }
        #endregion


        #region Métodos      
        #endregion


        #region Propiedades        
        /// <summary>
        /// Cada componente representa una electroválvula,
        /// las CINCO electroválvulas forman un MODULO COMPLETO
        /// </summary>
        #region COMPONENTES ELÉCTRICOS - ELECTROVÁLVULAS BARRIL (1,2 Y 3) - DETECTOR - GRIFO
        public Componentes ElectrovalvulaBarril1 { get; set; }
        public Componentes ElectrovalvulaBarril2 { get; set; }
        public Componentes ElectrovalvulaBarril3 { get; set; }
        public Componentes ElectrovalvulaDetector { get; set; }
        public Componentes ElectrovalvulaGrifo { get; set; }
        #endregion
        #region CONTENIDO DE BARRILES - 1 LINEA SON 3 BARRILES (1,2 Y 3)
        public Componentes Barril1 { get; set; }
        public Componentes Barril2 { get; set; }
        public Componentes Barril3 { get; set; }
        #endregion
        #endregion
    }
    #endregion


    #region CLASE ESPECÍFICA MODULOSTIPOA QUE HEREDA DE MODULOS
    /// <summary>
    /// Implementa un MODULO de clase MODULOSTIPOA
    /// </summary>
    public class ModulosTipoA : Modulos
    {
        #region Atributos
        #endregion


        #region Constructores
        public ModulosTipoA() : base()
        {
            #region MODULO A - ELECTROVALVULAS: BARRIL 1 - 2 - 3 / DETECTOR 4 / GRIFO 5
            ElectrovalvulaBarril1.Direccion = Constantes.DIRECCION_MEMORIA_EVA1_LINEA1_BARRIL1;
            ElectrovalvulaBarril2.Direccion = Constantes.DIRECCION_MEMORIA_EVA2_LINEA1_BARRIL2;
            ElectrovalvulaBarril3.Direccion = Constantes.DIRECCION_MEMORIA_EVA3_LINEA1_BARRIL3;
            ElectrovalvulaDetector.Direccion = Constantes.DIRECCION_MEMORIA_EVA4_LINEA1_DETECTOR;
            ElectrovalvulaGrifo.Direccion = Constantes.DIRECCION_MEMORIA_EVA5_LINEA1_GRIFO;
            #endregion

            #region CONTENIDO DE BARRILES - LINEA 1 SON 3 BARRILES (1,2 Y 3)
            Barril1.Direccion = Constantes.DIRECION_MEMORIA_BARRIL1_LLENO_LINEA1;
            Barril2.Direccion = Constantes.DIRECION_MEMORIA_BARRIL2_LLENO_LINEA1;
            Barril3.Direccion = Constantes.DIRECION_MEMORIA_BARRIL3_LLENO_LINEA1;
            #endregion
        }
        #endregion


        #region Métodos      
        #endregion


        #region Propiedades
        #endregion
    }
    #endregion


    #region CLASE ESPECÍFICA MODULOSTIPOB QUE HEREDA DE MODULOS
    /// <summary>
    /// Implementa un MODULO de clase MODULOSTIPOB
    /// </summary>
    public class ModulosTipoB : Modulos
    {
        #region Atributos
        #endregion


        #region Constructores
        public ModulosTipoB() : base()
        {
            #region MODULO B - ELECTROVALVULAS: BARRIL 11 - 12 - 13 / DETECTOR 14 / GRIFO 15
            ElectrovalvulaBarril1.Direccion = Constantes.DIRECCION_MEMORIA_EVA11_LINEA2_BARRIL1;
            ElectrovalvulaBarril2.Direccion = Constantes.DIRECCION_MEMORIA_EVA12_LINEA2_BARRIL2;
            ElectrovalvulaBarril3.Direccion = Constantes.DIRECCION_MEMORIA_EVA13_LINEA2_BARRIL3;
            ElectrovalvulaDetector.Direccion = Constantes.DIRECCION_MEMORIA_EVA14_LINEA2_DETECTOR;
            ElectrovalvulaGrifo.Direccion = Constantes.DIRECCION_MEMORIA_EVA15_LINEA2_GRIFO;
            #endregion

            #region CONTENIDO DE BARRILES - LINEA 2 SON 3 BARRILES (1,2 Y 3)
            Barril1.Direccion = Constantes.DIRECION_MEMORIA_BARRIL1_LLENO_LINEA2;
            Barril2.Direccion = Constantes.DIRECION_MEMORIA_BARRIL2_LLENO_LINEA2;
            Barril3.Direccion = Constantes.DIRECION_MEMORIA_BARRIL3_LLENO_LINEA2;
            #endregion
        }
        #endregion


        #region Métodos      
        #endregion


        #region Propiedades
        #endregion
    }
    #endregion


    #region CLASE ESPECÍFICA MODULOSTIPOC QUE HEREDA DE MODULOS
    /// <summary>
    /// Implementa un MODULO de clase MODULOSTIPOC
    /// </summary>
    public class ModulosTipoC : Modulos
    {
        #region Atributos
        #endregion


        #region Constructores
        public ModulosTipoC() : base()
        {
            #region MODULO C - ELECTROVALVULAS: BARRIL 21 - 22 - 23 / DETECTOR 24 / GRIFO 25
            ElectrovalvulaBarril1.Direccion = Constantes.DIRECCION_MEMORIA_EVA21_LINEA3_BARRIL1;
            ElectrovalvulaBarril2.Direccion = Constantes.DIRECCION_MEMORIA_EVA22_LINEA3_BARRIL2;
            ElectrovalvulaBarril3.Direccion = Constantes.DIRECCION_MEMORIA_EVA23_LINEA3_BARRIL3;
            ElectrovalvulaDetector.Direccion = Constantes.DIRECCION_MEMORIA_EVA24_LINEA3_DETECTOR;
            ElectrovalvulaGrifo.Direccion = Constantes.DIRECCION_MEMORIA_EVA25_LINEA3_GRIFO;
            #endregion

            #region CONTENIDO DE BARRILES - LINEA 3 SON 3 BARRILES (1,2 Y 3)
            Barril1.Direccion = Constantes.DIRECION_MEMORIA_BARRIL1_LLENO_LINEA3;
            Barril2.Direccion = Constantes.DIRECION_MEMORIA_BARRIL2_LLENO_LINEA3;
            Barril3.Direccion = Constantes.DIRECION_MEMORIA_BARRIL3_LLENO_LINEA3;
            #endregion
        }
        #endregion


        #region Métodos      
        #endregion


        #region Propiedades
        #endregion
    }
    #endregion
}
