﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logo2020.CORE
{
    public class Componentes
    {
        #region Atributos 
        /// <summary>
        /// Cada componente tendrá:
        ///     - Un VALOR NUMÉRICO
        ///     - Una DIRECCION DE MORIA en Logo     
        /// Valor numérico que representa el estado del componente
        /// </summary>
        private byte valor;        
        /// <summary>  
        /// Dirección de memoria de logo que guarda el valor anterior
        /// </summary>
        private int direccion;
        #endregion


        #region Constructores
        public Componentes() { }
        #endregion


        #region Métodos      
        #endregion


        #region Propiedades
        public int Direccion
        {
            get { return direccion; }
            set { direccion = value; }
        }
        public byte Valor
        {
            get { return valor; }
            set { valor = value; }
        }
        #endregion
    }
}
