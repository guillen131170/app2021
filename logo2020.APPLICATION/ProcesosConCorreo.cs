﻿using logo2020.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace logo2020.APPLICATION
{
    public class ProcesosConCorreo
    {
        /*################################################################*/
        #region ATRIBUTOS
        /*HILO PARA EL ENVÍO DEL CORREO ELECTRÓNICO*/
        private Thread hilo;

        public string asunto;
        public string cuerpo;
        public List<String> destino;
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region CONSTRUCTORES     
        /// <summary>
        /// Crea el hilo que envía el correo electrónico
        /// </summary>
        /// <param name="asunto"></param>
        /// <param name="cuerpo"></param>
        /// <param name="destino"></param>
        public ProcesosConCorreo(string asunto, string cuerpo)
        {
            #region INICIA LOS ATRIBUTOS
            this.asunto = asunto;
            this.cuerpo = cuerpo;

            ProcesosConSQL sql = new ProcesosConSQL();
            destino = sql.recuperarEmails();
            #endregion

            #region INICIO DEL HILO
            /*DELEGADO*/
            ThreadStart delegado = new ThreadStart(CorrerProceso);
            /*HILO*/
            hilo = new Thread(delegado);
            /*INICIO DE HILO*/
            hilo.Start();
            #endregion
        }
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region OPERACIONES REALIZADAS MIENTRAS EL HILO ESTÁ ACTIVO    
        /// <summary>
        /// PROCESOS DURANTE LA DURACIÓN DEL HILO
        /// </summary>
        private void CorrerProceso()
        {
            foreach (String s in destino)
            {
                enviarCorreo(s);
            }
            hilo.Abort();
        }
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region ENVÍA UN CORREO ELECTRÓNICO
        /// <summary>
        /// Envía un correo electrónico
        /// </summary>
        /// <param name="asunto">asunto del que se trata</param>
        /// <param name="cuerpo">texto a enviar</param>
        /// <param name="destinatario">a quién va dirigido</param>
        /// <returns></returns>
        public bool enviarCorreo(String address)
        {
            bool resultado = false;
            MailMessage mail = new MailMessage();
            mail.To.Add(address);
            mail.From = new MailAddress(Constantes.ADDRESS_MAIL_FROM);

            mail.Subject = asunto.Trim();
            mail.Body = cuerpo.Trim();
            mail.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.live.com";
            smtp.Port = 587;

            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new System.Net.NetworkCredential
            (Constantes.ADDRESS_MAIL_FROM, Constantes.PASSWORD_MAIL_FROM);
            smtp.EnableSsl = true;
            try
            {
                smtp.Send(mail);
                resultado = true;
            }
            catch (Exception) { }
            return resultado;
        }
        #endregion
        /*################################################################*/
    }
}
