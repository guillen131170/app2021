﻿using logo2020.IFR;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace logo2020.PRESENTATION
{
    public partial class FormConfAlarmas : Form
    {
        #region Atributos
        //Indica si logo está conectado
        private bool conectado;
        //Variables para recibir parámetros tipo Control
        public Button apagado;
        public Button encendido;
        #endregion


        #region Inicia y carga formulario
        #region Constructores
        /// <summary>
        /// Sin parámetro
        /// </summary>
        public FormConfAlarmas()
        {
            conectado = false;
            InitializeComponent();
        }

        /// <summary>
        /// Con parámetros
        /// </summary>
        /// <param name="conectado">¿Está conectado logo?</param>
        /// <param name="off">Botón apagado</param>
        /// <param name="on">Botón encendido</param>
        public FormConfAlarmas(bool conectado, Button off, Button on)
        {
            encendido = on;
            apagado = off;
            this.conectado = conectado;
            InitializeComponent();
        }
        #endregion

        /// <summary>
        /// Carga el formulario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormConfAlarmas_Load(object sender, EventArgs e)
        {

        }
        #endregion


        #region EVENTOS
        /// <summary>
        /// Cierra el formulario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cerrarConfAlarmas_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Cambia el estado de las alarmas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void enviarConfAlarmas_Click(object sender, EventArgs e)
        {
            if (!conectado) //Si no está conectado
            {
                contrato.CambiarEstadoAlarmas(checkco2.Checked, checkpuerta.Checked,
                                              checkcompresor.Checked, checkfusible.Checked);
            }
            else //Si está conectado
            {
                //Desconecta, cambiar el número de grifos y vuelve a conectar
                apagado.PerformClick();
                contrato.CambiarEstadoAlarmas(checkco2.Checked, checkpuerta.Checked,
                                              checkcompresor.Checked, checkfusible.Checked);
                encendido.PerformClick();
            }
            this.Close();
        }
        #endregion

        #region PROPIEDAD DE INTERFACE IContract
        /// <summary>
        /// PROPIEDAD PARA INTERFACE IContract
        /// </summary>
        public IContract contrato { get; set; }
        #endregion
    }
}
