﻿using logo2020.APPLICATION;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace logo2020.PRESENTATION
{
    public partial class FormVerificarBD : Form
    {
        private bool conectadoBD;

        public FormVerificarBD(ref bool c)
        {
            InitializeComponent();
            conectadoBD = c;
        }

        private void FormVerificarBD_Load(object sender, EventArgs e)
        {
            labelconectado.Text = "";
            labelconectado.ForeColor = Color.Black;
            linea.ForeColor = Color.Black;
            linea.BackColor = Color.Black;
            pictureBoxpc.Image = Properties.Resources.Laptop_OFF;
            pictureBoxbd.Image = Properties.Resources.bd_off;
        }


        private void cerrarbd_Click(object sender, EventArgs e)
        {
            labelconectado.Text = "";
            labelconectado.ForeColor = Color.Black;
            linea.ForeColor = Color.Black;
            linea.BackColor = Color.Black;
            pictureBoxpc.Image = Properties.Resources.Laptop_OFF;
            pictureBoxbd.Image = Properties.Resources.bd_off;
            this.Close();
        }

        private void probar_Click(object sender, EventArgs e)
        {
            labelconectado.Text = "";
            labelconectado.ForeColor = Color.Black;
            linea.ForeColor = Color.Black;
            linea.BackColor = Color.Black;
            pictureBoxpc.Image = Properties.Resources.Laptop_OFF;
            pictureBoxbd.Image = Properties.Resources.bd_off;

            //COMPRUEBA SI HAY BASE DE DATOS
            ProcesosConSQL sql = new ProcesosConSQL();
            if (sql.verificar())
            {
                conectadoBD = true;
                labelconectado.ForeColor = Color.Green;
                labelconectado.Text = "CONECTADO";
                linea.ForeColor = Color.Lime;
                linea.BackColor = Color.Lime;
                pictureBoxpc.Image = Properties.Resources.Laptop_ON;
                pictureBoxbd.Image = Properties.Resources.bd_on;
            }
            else
            {
                conectadoBD = false;
                labelconectado.ForeColor = Color.Red;
                labelconectado.Text = "NO CONECTADO";
                linea.ForeColor = Color.Red;
                linea.BackColor = Color.Red;
                pictureBoxpc.Image = Properties.Resources.Laptop_OFF;
                pictureBoxbd.Image = Properties.Resources.bd_off;
            }
        }
    }
}
