﻿namespace logo2020.PRESENTATION
{
    partial class FormConfAlarmas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cerrarConfAlarmas = new System.Windows.Forms.Button();
            this.enviarConfAlarmas = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkfusible = new System.Windows.Forms.CheckBox();
            this.checkcompresor = new System.Windows.Forms.CheckBox();
            this.checkpuerta = new System.Windows.Forms.CheckBox();
            this.checkco2 = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cerrarConfAlarmas
            // 
            this.cerrarConfAlarmas.FlatAppearance.BorderSize = 0;
            this.cerrarConfAlarmas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cerrarConfAlarmas.Image = global::logo2020.PRESENTATION.Properties.Resources.CERRAR;
            this.cerrarConfAlarmas.Location = new System.Drawing.Point(16, 260);
            this.cerrarConfAlarmas.Name = "cerrarConfAlarmas";
            this.cerrarConfAlarmas.Size = new System.Drawing.Size(75, 40);
            this.cerrarConfAlarmas.TabIndex = 40;
            this.cerrarConfAlarmas.UseVisualStyleBackColor = true;
            this.cerrarConfAlarmas.Visible = false;
            this.cerrarConfAlarmas.Click += new System.EventHandler(this.cerrarConfAlarmas_Click);
            // 
            // enviarConfAlarmas
            // 
            this.enviarConfAlarmas.FlatAppearance.BorderSize = 0;
            this.enviarConfAlarmas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.enviarConfAlarmas.Image = global::logo2020.PRESENTATION.Properties.Resources.enviar;
            this.enviarConfAlarmas.Location = new System.Drawing.Point(486, 260);
            this.enviarConfAlarmas.Name = "enviarConfAlarmas";
            this.enviarConfAlarmas.Size = new System.Drawing.Size(75, 40);
            this.enviarConfAlarmas.TabIndex = 39;
            this.enviarConfAlarmas.UseVisualStyleBackColor = true;
            this.enviarConfAlarmas.Click += new System.EventHandler(this.enviarConfAlarmas_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.checkfusible);
            this.panel1.Controls.Add(this.checkcompresor);
            this.panel1.Controls.Add(this.checkpuerta);
            this.panel1.Controls.Add(this.checkco2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cerrarConfAlarmas);
            this.panel1.Controls.Add(this.enviarConfAlarmas);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(566, 305);
            this.panel1.TabIndex = 41;
            // 
            // checkfusible
            // 
            this.checkfusible.AutoSize = true;
            this.checkfusible.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkfusible.Location = new System.Drawing.Point(201, 200);
            this.checkfusible.Name = "checkfusible";
            this.checkfusible.Size = new System.Drawing.Size(147, 22);
            this.checkfusible.TabIndex = 50;
            this.checkfusible.Text = "Fusible General";
            this.checkfusible.UseVisualStyleBackColor = true;
            // 
            // checkcompresor
            // 
            this.checkcompresor.AutoSize = true;
            this.checkcompresor.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkcompresor.Location = new System.Drawing.Point(201, 160);
            this.checkcompresor.Name = "checkcompresor";
            this.checkcompresor.Size = new System.Drawing.Size(163, 22);
            this.checkcompresor.TabIndex = 49;
            this.checkcompresor.Text = "Compresor de Aire";
            this.checkcompresor.UseVisualStyleBackColor = true;
            // 
            // checkpuerta
            // 
            this.checkpuerta.AutoSize = true;
            this.checkpuerta.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkpuerta.Location = new System.Drawing.Point(201, 120);
            this.checkpuerta.Name = "checkpuerta";
            this.checkpuerta.Size = new System.Drawing.Size(139, 22);
            this.checkpuerta.TabIndex = 48;
            this.checkpuerta.Text = "Puerta Abierta";
            this.checkpuerta.UseVisualStyleBackColor = true;
            // 
            // checkco2
            // 
            this.checkco2.AutoSize = true;
            this.checkco2.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkco2.Location = new System.Drawing.Point(201, 80);
            this.checkco2.Name = "checkco2";
            this.checkco2.Size = new System.Drawing.Size(51, 22);
            this.checkco2.TabIndex = 47;
            this.checkco2.Text = "CO2";
            this.checkco2.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(472, 36);
            this.label1.TabIndex = 43;
            this.label1.Text = "POR FAVOR,\r\nMARQUE LAS CASILLAS PARA ACTIVAR O DESACTIVAR LAS ALAMRMAS";
            // 
            // FormConfAlarmas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(590, 329);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormConfAlarmas";
            this.Text = "CONFIGURACIÓN DE ALARMAS DEL SISTEMA";
            this.Load += new System.EventHandler(this.FormConfAlarmas_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button cerrarConfAlarmas;
        private System.Windows.Forms.Button enviarConfAlarmas;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkfusible;
        private System.Windows.Forms.CheckBox checkcompresor;
        private System.Windows.Forms.CheckBox checkpuerta;
        private System.Windows.Forms.CheckBox checkco2;
    }
}