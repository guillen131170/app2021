﻿using logo2020.APPLICATION;
using logo2020.CORE;
using logo2020.CORE.GrupoComponentes;
using logo2020.IFR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace logo2020.PRESENTATION
{
    public class Hilos
    {
        /*################################################################*/
        #region Atributos
        /* Hilo para Conexión */
        private Thread hilo;

        /* PLC del hilo */
        /// <summary>
        /// Ejecuta las operaciones con Logo
        /// </summary>
        private ProcesosConLogo proceso;
        /// <summary>
        /// Ejecuta las operaciones con AQL
        /// </summary>
        private ProcesosConSQL sql;
        /// <summary>
        /// Formulario
        /// </summary>
        private Form1 f;
        /// <summary>
        /// Indica estado de conexion con logo
        /// </summary>
        private bool conectadoLogo;
        /// <summary>
        /// Indica estado de conexion con SQL
        /// </summary>
        private bool conectadoBD;
        /// <summary>
        /// Indica el número de grifos del sistema
        /// </summary>
        private int nGrifos;
        /// <summary>
        /// Recoge el estado de las alarmas - para lanzarlas o no
        /// </summary>
        public EstadoAlarmas estadoAlarmas;

        private const int NUMERO_BYTES = 512;
        private byte[] memoriaLogo;
        /*FORMATO AAAAMMDD*/
        private int fecha;

        #region Componentes eléctricos
        /// <summary>
        /// Este objeto representa los componentes eléctricos
        /// y guarda las direcciones de memoria de Logo
        /// junto con sus valores
        /// </summary>
        private Ensamblados ensamblado;
        #endregion

        #region CONSUMOS - VENTAS
        private int[] barrilActivo = new int[3];

        #region CODIGO DE PRUEBAS
        /// <summary>
        /// Contiene las ventas de las tres lineas - barriles y litros
        /// </summary>
        private Ventas ventas; 
        #endregion
        #endregion
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region /* Constructores */
        /// <summary>
        /// Crea el hilo que mantiene la conexión con Logo
        /// </summary>
        /// <param name="_proceso">OPERACIONES CON LOGO</param>
        /// <param name="_form">PANTALLA HMI</param>
        public Hilos(ProcesosConLogo _proceso, Form1 _form, ref bool _conectado, ref bool _bd, 
                     int ngrifos, EstadoAlarmas estadoAlarmas)
        {
            //------------------------------------------------
            #region INICIA LOS OBJETOS
            f = _form;
            proceso = _proceso;
            sql = new ProcesosConSQL();
            ConectadoLogo = _conectado;
            ConectadoBD = _bd;
            NGrifos = ngrifos;
            memoriaLogo = new byte[NUMERO_BYTES];
            ensamblado = new Ensamblados();
            this.estadoAlarmas = estadoAlarmas;
            Venta = new Ventas();
            Venta.Consumo = new Consumos();

            /*Inicia fecha para comprobar consumos*/
            DateTime fechaActual = DateTime.Today;
            int fecha = (fechaActual.Year * 10000) +
                         (fechaActual.Month * 100) +
                         (fechaActual.Day);
            #endregion
            //------------------------------------------------


            //------------------------------------------------
            #region INICIO DEL HILO
            /*DELEGADO*/
            ThreadStart delegado = new ThreadStart(CorrerProceso);
            /*HILO*/
            hilo = new Thread(delegado);
            /*INICIO DE HILO*/
            hilo.Start();
            #endregion
            //------------------------------------------------
        }
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region Proceso del hilo
        /// <summary>
        /// Todas las tareas que se realizan dentro del hilo
        /// </summary>
        private void CorrerProceso()
        {
            //------------------------------------------------
            #region OPERACIONES ANTERIORES AL INICIO DEL BUCLE DEL HILO - PREPARAR HILO
            if (ConectadoBD)
                Log.LogInfo(DateTime.Now + " SE HA INICIADO UN SUBPROCESO CON EL LOGO");
            f.escribirDisplay(DateTime.Now + " SE HA INICIADO UN SUBPROCESO CON EL LOGO");

            #region LECTURA DE MEMORIA DE LOGO Y DIBUJO DE HMI
            #region LECTURA DE MEMORIA DE LOGO
            try
            {
                //Lectura de memoria de logo
                proceso.LeerBloqueAB();
                int i = 0;
                foreach (byte b in proceso.buffer512)
                {
                    memoriaLogo[i] = b;
                    i++;
                }
                //Actualiza celdas de memoria de ensamblado
                ensamblado.ActualizaValores(memoriaLogo);
                //Copia toda la memoria de logo en ensamblado
                ensamblado.ActualizaMemoria(memoriaLogo);
            }
            catch (Exception ex)
            {
                if (ConectadoBD)
                    Log.LogWarn("ERROR AL LEER EN MEMORIA DE LOGO - LeerBloque512(): " + ex.Message);
            }
            #endregion

            #region ASIGNA VALORES INICIALES PARA MOSTRAR CONSUMOS
            #region OBTIENE LOS BARRILES ACTIVOS DE CADA LINEA - VALORES INICIALES
            Venta.barrilActivoInicial[0] = proceso.activo(Constantes.DIRECCION_MEMORIA_EVA1_LINEA1_BARRIL1);
            Venta.barrilActivoInicial[1] = proceso.activo(Constantes.DIRECCION_MEMORIA_EVA11_LINEA2_BARRIL1);
            Venta.barrilActivoInicial[2] = proceso.activo(Constantes.DIRECCION_MEMORIA_EVA21_LINEA3_BARRIL1);
            #endregion

            #region OBTIENE EL CONTENIDO EN LITROS DE CADA BARRIL ACTIVO - VALORES INICIALES
            Venta.numLitrosBarrilActivoInicial[0] = proceso.ObtieneNumLitros(Constantes.N_LITROS_BARRIL1_LINEA1_BYTE_0);
            Venta.numLitrosBarrilActivoInicial[1] = proceso.ObtieneNumLitros(Constantes.N_LITROS_BARRIL2_LINEA1_BYTE_0);
            Venta.numLitrosBarrilActivoInicial[2] = proceso.ObtieneNumLitros(Constantes.N_LITROS_BARRIL3_LINEA1_BYTE_0);
            #endregion

            #region OBTIENE EL CONTENIDO EN LITROS DE TODOS LOS BARRILES - VALORES INICIALES
            Venta.numLitros[0, 0] = proceso.ObtieneNumLitros(Constantes.N_LITROS_BARRIL1_LINEA1_BYTE_0);
            Venta.numLitros[0, 1] = proceso.ObtieneNumLitros(Constantes.N_LITROS_BARRIL2_LINEA1_BYTE_0);
            Venta.numLitros[0, 2] = proceso.ObtieneNumLitros(Constantes.N_LITROS_BARRIL3_LINEA1_BYTE_0);
            Venta.numLitros[1, 0] = proceso.ObtieneNumLitros(Constantes.N_LITROS_BARRIL1_LINEA2_BYTE_0);
            Venta.numLitros[1, 1] = proceso.ObtieneNumLitros(Constantes.N_LITROS_BARRIL2_LINEA2_BYTE_0);
            Venta.numLitros[1, 2] = proceso.ObtieneNumLitros(Constantes.N_LITROS_BARRIL3_LINEA2_BYTE_0);
            Venta.numLitros[2, 0] = proceso.ObtieneNumLitros(Constantes.N_LITROS_BARRIL1_LINEA3_BYTE_0);
            Venta.numLitros[2, 1] = proceso.ObtieneNumLitros(Constantes.N_LITROS_BARRIL2_LINEA3_BYTE_0);
            Venta.numLitros[2, 2] = proceso.ObtieneNumLitros(Constantes.N_LITROS_BARRIL3_LINEA3_BYTE_0);
            #endregion

            #region REALIZA EL RECUENTO DE BARRILES VentaParcialS DE CADA UNA DE LAS LÍNEAS - VALORES INICIALES
            #region CONTADOR DE LINEA 1
            Venta.numBarrilesInicial[0] = proceso.ObtieneNumBarriles(Constantes.N_BARRILES_LINEA1_BYTE_1);
            #endregion
            #region CONTADOR DE LINEA 2
            if (NGrifos > 1)
            {
                Venta.numBarrilesInicial[1] = proceso.ObtieneNumBarriles(Constantes.N_BARRILES_LINEA2_BYTE_1);
            }
            #endregion
            #region CONTADOR DE LINEA 3
            if (NGrifos > 2)
            {
                Venta.numBarrilesInicial[2] = proceso.ObtieneNumBarriles(Constantes.N_BARRILES_LINEA3_BYTE_1);
            }
            #endregion           
            #endregion
            #endregion

            #region COMPRUEBA SI HAY REGISTRO DE CONSUMO DEL DÍA ACTUAL Y CAMBIA VALOR DE LA VARIABLE           
            #region OBTIENE LA FECHA ACTUAL EN EL FORMATO ADECUADO
            DateTime fechaActual = DateTime.Today;
            int _fecha = (fechaActual.Year * 10000) +
                         (fechaActual.Month * 100) +
                         (fechaActual.Day);
            #endregion
            #region COMPRUEBA SI EXISTE UN REGISTRO DE CONSUMO EN EL DÍA ACTUAL
            bool existeregistro = false;
            if (conectadoBD)
            {
                if (sql.comprobarConsumo(_fecha))
                {
                    existeregistro = true;
                }
            }
            #endregion
            #endregion

            #region DIBUJA HMI INICIAL - ANTES DEL BUCLE DEL HILO
            f.dibujaEVA123_1();
            f.dibujaEVA123_2();
            f.dibujaEVA123_3();
            f.dibujaEVA5_1();
            f.dibujaEVA5_2();
            f.dibujaEVA5_3();
            f.dibujaDetector1();
            f.dibujaDetector2();
            f.dibujaDetector3();
            f.ParoMarcha_1();
            f.ParoMarcha_2();
            f.ParoMarcha_3();
            f.dibujaAlarmaCO2();
            f.dibujaAlarmaCA();
            f.dibujaAlarmaPuerta();
            f.dibujaAlarmaFusible();
            f.dibujaLitros(Venta.numLitros);
            f.dibujaConsumoTotal(Venta.numBarrilesInicial[0], Venta.numBarrilesInicial[1], Venta.numBarrilesInicial[2]);
            #endregion
            #endregion 
            #endregion
            //------------------------------------------------


            //------------------------------------------------
            #region OPERACIONES DURANTE EL BUCLE DEL HILO - INICIO HILO
            while (ConectadoLogo && proceso.EstaConectado())
            {
                #region BUCLE DEL HILO
                #region MIENTRAS EXISTA CONEXION CON LOGO    
                int Resultado = 0;

                #region LECTURA DE MEMORIA DE LOGO
                try
                {
                    Resultado = proceso.LeerBloqueAB();
                    int i = 0;
                    foreach (byte b in proceso.buffer512)
                    {
                        memoriaLogo[i] = b;
                        i++;
                    }
                    f.escribirPlc(proceso.Client.ExecutionTime.ToString() + " MS");
                }
                catch (Exception ex)
                {
                    if (ConectadoBD)
                        Log.LogWarn("ERROR AL LEER EN MEMORIA DE LOGO - LeerBloque512(): " + ex.Message);
                }
                #endregion


                #region OBTIENE EL NUMERO DE BARRILES VentaParcialS DE CADA UNA DE LAS LÍNEAS Y DIBUJA
                #region CONTADOR DE LINEA 1
                Venta.numBarrilesActual[0] = proceso.ObtieneNumBarriles(Constantes.N_BARRILES_LINEA1_BYTE_1);
                Venta.Consumo.VentaTotalGrifo1 = Venta.numBarrilesActual[0];
                #endregion
                #region CONTADOR DE LINEA 2
                if (NGrifos > 1)
                {
                    Venta.numBarrilesActual[1] = proceso.ObtieneNumBarriles(Constantes.N_BARRILES_LINEA2_BYTE_1);
                    Venta.Consumo.VentaTotalGrifo2 = Venta.numBarrilesActual[1];
                }
                #endregion
                #region CONTADOR DE LINEA 3
                if (NGrifos > 2)
                {
                    Venta.numBarrilesActual[2] = proceso.ObtieneNumBarriles(Constantes.N_BARRILES_LINEA3_BYTE_1);
                    Venta.Consumo.VentaTotalGrifo3 = Venta.numBarrilesActual[2];
                }
                #endregion
                #region TODOS LOS GRIFOS
                Venta.Consumo.VentaTotal = Venta.Consumo.VentaTotalGrifo1 +
                           Venta.Consumo.VentaTotalGrifo2 +
                           Venta.Consumo.VentaTotalGrifo3; 
                #endregion
                f.dibujaConsumoTotal(Venta.numBarrilesActual[0], Venta.numBarrilesActual[1], Venta.numBarrilesActual[2]);
                #endregion


                #region OBTIENE EL NUMERO DE LITROS DE TODOS LOS BARRILES Y DIBUJA
                #region OBTIENE EL CONTENIDO EN LITROS DE TODOS LOS BARRILES
                Venta.numLitros[0, 0] = proceso.ObtieneNumLitros(Constantes.N_LITROS_BARRIL1_LINEA1_BYTE_0);
                Venta.numLitros[0, 1] = proceso.ObtieneNumLitros(Constantes.N_LITROS_BARRIL2_LINEA1_BYTE_0);
                Venta.numLitros[0, 2] = proceso.ObtieneNumLitros(Constantes.N_LITROS_BARRIL3_LINEA1_BYTE_0);
                Venta.numLitros[1, 0] = proceso.ObtieneNumLitros(Constantes.N_LITROS_BARRIL1_LINEA2_BYTE_0);
                Venta.numLitros[1, 1] = proceso.ObtieneNumLitros(Constantes.N_LITROS_BARRIL2_LINEA2_BYTE_0);
                Venta.numLitros[1, 2] = proceso.ObtieneNumLitros(Constantes.N_LITROS_BARRIL3_LINEA2_BYTE_0);
                Venta.numLitros[2, 0] = proceso.ObtieneNumLitros(Constantes.N_LITROS_BARRIL1_LINEA3_BYTE_0);
                Venta.numLitros[2, 1] = proceso.ObtieneNumLitros(Constantes.N_LITROS_BARRIL2_LINEA3_BYTE_0);
                Venta.numLitros[2, 2] = proceso.ObtieneNumLitros(Constantes.N_LITROS_BARRIL3_LINEA3_BYTE_0);
                #endregion
                f.dibujaLitros(Venta.numLitros);
                #endregion


                #region GRIFO 1
                #region comprueba - electroválvulas 1,2 y 3
                if (ensamblado.ModuloA.ElectrovalvulaBarril1.Valor != memoriaLogo[Constantes.DIRECCION_MEMORIA_EVA1_LINEA1_BARRIL1] ||
                    ensamblado.ModuloA.ElectrovalvulaBarril2.Valor != memoriaLogo[Constantes.DIRECCION_MEMORIA_EVA2_LINEA1_BARRIL2] ||
                    ensamblado.ModuloA.ElectrovalvulaBarril3.Valor != memoriaLogo[Constantes.DIRECCION_MEMORIA_EVA3_LINEA1_BARRIL3])
                {
                    f.dibujaEVA123_1();
                }
                #endregion
                #region comprueba barriles pinchados - linea 1
                if (ensamblado.ModuloA.Barril1.Valor != memoriaLogo[Constantes.DIRECION_MEMORIA_BARRIL1_LLENO_LINEA1] ||
                    ensamblado.ModuloA.Barril2.Valor != memoriaLogo[Constantes.DIRECION_MEMORIA_BARRIL2_LLENO_LINEA1] ||
                    ensamblado.ModuloA.Barril3.Valor != memoriaLogo[Constantes.DIRECION_MEMORIA_BARRIL3_LLENO_LINEA1])
                {
                    f.dibujaEVA123_1();
                }
                #endregion
                #region comprueba - electroválvulas 5
                if (ensamblado.ModuloA.ElectrovalvulaGrifo.Valor != memoriaLogo[Constantes.DIRECCION_MEMORIA_EVA5_LINEA1_GRIFO])
                {
                    f.dibujaEVA5_1();
                }
                #endregion
                #region comprueba - detectores1
                if (ensamblado.ModuloA.ElectrovalvulaDetector.Valor != memoriaLogo[Constantes.DIRECCION_MEMORIA_EVA4_LINEA1_DETECTOR])
                {
                    f.dibujaDetector1();
                }
                #endregion
                #endregion


                #region GRIFO 2
                if (NGrifos > 1)
                {
                    #region comprueba - electroválvulas 11,12 y 13
                    if (ensamblado.ModuloB.ElectrovalvulaBarril1.Valor != memoriaLogo[Constantes.DIRECCION_MEMORIA_EVA11_LINEA2_BARRIL1] ||
                        ensamblado.ModuloB.ElectrovalvulaBarril2.Valor != memoriaLogo[Constantes.DIRECCION_MEMORIA_EVA12_LINEA2_BARRIL2] ||
                        ensamblado.ModuloB.ElectrovalvulaBarril3.Valor != memoriaLogo[Constantes.DIRECCION_MEMORIA_EVA13_LINEA2_BARRIL3])
                    {
                        f.dibujaEVA123_2();
                    }
                    #endregion
                    #region comprueba barriles pinchados - linea 2
                    if (ensamblado.ModuloB.Barril1.Valor != memoriaLogo[Constantes.DIRECION_MEMORIA_BARRIL1_LLENO_LINEA2] ||
                        ensamblado.ModuloB.Barril2.Valor != memoriaLogo[Constantes.DIRECION_MEMORIA_BARRIL2_LLENO_LINEA2] ||
                        ensamblado.ModuloB.Barril3.Valor != memoriaLogo[Constantes.DIRECION_MEMORIA_BARRIL3_LLENO_LINEA2])
                    {
                        f.dibujaEVA123_2();
                    }
                    #endregion
                    #region comprueba - electroválvulas 15
                    if (ensamblado.ModuloB.ElectrovalvulaGrifo.Valor != memoriaLogo[Constantes.DIRECCION_MEMORIA_EVA15_LINEA2_GRIFO])
                    {
                        f.dibujaEVA5_2();
                    }
                    #endregion
                    #region comprueba - detectores2
                    if (ensamblado.ModuloB.ElectrovalvulaDetector.Valor != memoriaLogo[Constantes.DIRECCION_MEMORIA_EVA14_LINEA2_DETECTOR])
                    {
                        f.dibujaDetector2();
                    }
                    #endregion
                }
                #endregion


                #region GRIFO 3
                if (NGrifos > 2)
                {
                    #region comprueba - electroválvulas 21,22 y 23
                    if (ensamblado.ModuloC.ElectrovalvulaBarril1.Valor != memoriaLogo[Constantes.DIRECCION_MEMORIA_EVA21_LINEA3_BARRIL1] ||
                        ensamblado.ModuloC.ElectrovalvulaBarril2.Valor != memoriaLogo[Constantes.DIRECCION_MEMORIA_EVA22_LINEA3_BARRIL2] ||
                        ensamblado.ModuloC.ElectrovalvulaBarril3.Valor != memoriaLogo[Constantes.DIRECCION_MEMORIA_EVA23_LINEA3_BARRIL3])
                    {
                        f.dibujaEVA123_3();
                    }
                    #endregion
                    #region comprueba barriles pinchados - linea 3
                    if (ensamblado.ModuloC.Barril1.Valor != memoriaLogo[Constantes.DIRECION_MEMORIA_BARRIL1_LLENO_LINEA3] ||
                        ensamblado.ModuloC.Barril2.Valor != memoriaLogo[Constantes.DIRECION_MEMORIA_BARRIL2_LLENO_LINEA3] ||
                        ensamblado.ModuloC.Barril3.Valor != memoriaLogo[Constantes.DIRECION_MEMORIA_BARRIL3_LLENO_LINEA3])
                    {
                        f.dibujaEVA123_3();
                    }
                    #endregion
                    #region comprueba - electroválvulas 25
                    if (ensamblado.ModuloC.ElectrovalvulaGrifo.Valor != memoriaLogo[Constantes.DIRECCION_MEMORIA_EVA25_LINEA3_GRIFO])
                    {
                        f.dibujaEVA5_3();
                    }
                    #endregion
                    #region comprueba - detectores3
                    if (ensamblado.ModuloC.ElectrovalvulaDetector.Valor != memoriaLogo[Constantes.DIRECCION_MEMORIA_EVA24_LINEA3_DETECTOR])
                    {
                        f.dibujaDetector3();
                    }
                    #endregion
                }
                #endregion


                #region PARO/MARCHA
                #region PARO/MARCHA: LINEA 1
                if (ensamblado.ModuloA.ElectrovalvulaBarril1.Valor != memoriaLogo[310])
                {
                    f.ParoMarcha_1();
                }
                #endregion

                #region PARO/MARCHA: LINEA 2
                if (NGrifos > 1)
                {
                    if (ensamblado.ModuloB.ElectrovalvulaBarril1.Valor != memoriaLogo[320])
                    {
                        f.ParoMarcha_2();
                    }
                }
                #endregion

                #region PARO/MARCHA: LINEA 3
                if (NGrifos > 2)
                {
                    if (ensamblado.ModuloC.ElectrovalvulaBarril1.Valor != memoriaLogo[330])
                    {
                        f.ParoMarcha_3();
                    }
                }
                #endregion
                #endregion


                #region ALARMAS
                #region comprueba - alarma co2
                if (ensamblado.Alarma.Co2.Valor != memoriaLogo[Constantes.DIRECCION_MEMORIA_ALARMA_CO2])
                {
                    f.dibujaAlarmaCO2();
                }

                #region COMPRUEBA SI LA ALARMA ESTA ACTIVADA
                if (estadoAlarmas.Co2)
                {
                    if (ensamblado.Alarma.Co2.Valor != Constantes.VALOR_OK_ALARMA_CO2)
                    {
                        if (ensamblado.Alarma.Auxiliar_co2 == 0)
                        {
                            ensamblado.Alarma.Auxiliar_co2 = 255;
                            /* ENVÍA CORREO */
                            ProcesosConCorreo envio = new ProcesosConCorreo("INSTALACIÓN SIN CO2",
                                                                            "REPONGA CO2");
                        }
                    }
                    else
                    {
                        if (ensamblado.Alarma.Auxiliar_co2 != 0)
                        {
                            ensamblado.Alarma.Auxiliar_co2 = 0;
                        }
                    }
                }
                #endregion
                #endregion

                #region comprueba - alarma del compresor de aire
                if (ensamblado.Alarma.Compresor.Valor != memoriaLogo[Constantes.DIRECCION_MEMORIA_ALARMA_COMPRESOR_AIRE])
                {
                    f.dibujaAlarmaCA();
                }

                #region COMPRUEBA SI LA ALARMA ESTA ACTIVADA
                if (estadoAlarmas.Compresor)
                {
                    if (ensamblado.Alarma.Compresor.Valor != Constantes.VALOR_OK_ALARMA_COMPRESOR_AIRE)
                    {
                        if (ensamblado.Alarma.Auxiliar_compresor == 0)
                        {
                            ensamblado.Alarma.Auxiliar_compresor = 255;
                            /* ENVÍA CORREO */
                            ProcesosConCorreo envio = new ProcesosConCorreo("COMPRESOR AVERIADO",
                                                                            "REVISAR COMPRESOR DE AIRE");
                        }
                    }
                    else
                    {
                        if (ensamblado.Alarma.Auxiliar_compresor != 0)
                        {
                            ensamblado.Alarma.Auxiliar_compresor = 0;
                        }
                    }
                }
                #endregion
                #endregion

                #region comprueba - alarma puerta abierta
                if (ensamblado.Alarma.Puerta.Valor != memoriaLogo[Constantes.DIRECCION_MEMORIA_ALARMA_PUERTA_ABIERTA])
                {
                    f.dibujaAlarmaPuerta();
                }

                #region COMPRUEBA SI LA ALARMA ESTA ACTIVADA
                if (estadoAlarmas.Puerta)
                {
                    if (ensamblado.Alarma.Puerta.Valor != Constantes.VALOR_OK_ALARMA_PUERTA_ABIERTA)
                    {
                        if (ensamblado.Alarma.Auxiliar_puerta == 0)
                        {
                            ensamblado.Alarma.Auxiliar_puerta = 255;
                            /* ENVÍA CORREO */
                            ProcesosConCorreo envio = new ProcesosConCorreo("PUERTA ABIERTA",
                                                                            "CERRAR PUERTA DE CAMARA");
                        }
                    }
                    else
                    {
                        if (ensamblado.Alarma.Auxiliar_puerta != 0)
                        {
                            ensamblado.Alarma.Auxiliar_puerta = 0;
                        }
                    }
                }
                #endregion
                #endregion

                #region comprueba - alarma fusible general
                if (ensamblado.Alarma.Fusible.Valor != memoriaLogo[Constantes.DIRECCION_MEMORIA_ALARMA_FUSIBLE])
                {
                    f.dibujaAlarmaFusible();
                }
                #region COMPRUEBA SI LA ALARMA ESTA ACTIVADA
                if (estadoAlarmas.Fusible)
                {
                    if (ensamblado.Alarma.Fusible.Valor != Constantes.VALOR_OK_ALARMA_FUSIBLE)
                    {
                        if (ensamblado.Alarma.Auxiliar_fusible == 0)
                        {
                            ensamblado.Alarma.Auxiliar_fusible = 255;
                            /* ENVÍA CORREO */
                            ProcesosConCorreo envio = new ProcesosConCorreo("FUSIBLE AVERIADO",
                                                                            "CAMBIAR FUSIBLE");
                        }
                    }
                    else
                    {
                        if (ensamblado.Alarma.Auxiliar_fusible != 0)
                        {
                            ensamblado.Alarma.Auxiliar_fusible = 0;
                        }
                    }
                }
                #endregion
                #endregion
                #endregion


                #region DIBUJA VENTAS
                #region OBTIENE LOS BARRILES ACTIVOS DE CADA LINEA
                Venta.barrilActivoActual[0] = proceso.activo(Constantes.DIRECCION_MEMORIA_EVA1_LINEA1_BARRIL1);
                Venta.barrilActivoActual[1] = proceso.activo(Constantes.DIRECCION_MEMORIA_EVA11_LINEA2_BARRIL1);
                Venta.barrilActivoActual[2] = proceso.activo(Constantes.DIRECCION_MEMORIA_EVA21_LINEA3_BARRIL1);
                #endregion

                #region OBTIENE EL CONTENIDO EN LITROS DE CADA BARRIL ACTIVO
                Venta.numLitrosBarrilActivoActual[0] = proceso.ObtieneNumLitros(Constantes.N_LITROS_BARRIL1_LINEA1_BYTE_0);
                Venta.numLitrosBarrilActivoActual[1] = proceso.ObtieneNumLitros(Constantes.N_LITROS_BARRIL2_LINEA1_BYTE_0);
                Venta.numLitrosBarrilActivoActual[2] = proceso.ObtieneNumLitros(Constantes.N_LITROS_BARRIL3_LINEA1_BYTE_0);
                #endregion

                #region ASIGNA VALORES INICIALES PARA MOSTRAR VENTAS
                #region GRIFO 1
                if (Venta.barrilActivoInicial[0] != Venta.barrilActivoActual[0])
                {
                    Venta.barrilActivoInicial[0] = Venta.barrilActivoActual[0];
                    Venta.numLitrosBarrilActivoInicial[0] = Venta.numLitrosBarrilActivoActual[0];
                }
                else
                {
                    Venta.Consumo.VentaParcialGrifo1 += (Venta.numLitrosBarrilActivoInicial[0] -
                                                         Venta.numLitrosBarrilActivoActual[0]);
                    Venta.numLitrosBarrilActivoInicial[0] = Venta.numLitrosBarrilActivoActual[0];
                }
                #endregion

                #region GRIFO 2
                if (Venta.barrilActivoInicial[1] != Venta.barrilActivoActual[1])
                {
                    Venta.barrilActivoInicial[1] = Venta.barrilActivoActual[1];
                    Venta.numLitrosBarrilActivoInicial[1] = Venta.numLitrosBarrilActivoActual[1];
                }
                else
                {
                    Venta.Consumo.VentaParcialGrifo2 += (Venta.numLitrosBarrilActivoInicial[1] -
                                                         Venta.numLitrosBarrilActivoActual[1]);
                    Venta.numLitrosBarrilActivoInicial[1] = Venta.numLitrosBarrilActivoActual[1];
                }
                #endregion

                #region GRIFO 3
                if (Venta.barrilActivoInicial[2] != Venta.barrilActivoActual[2])
                {
                    Venta.barrilActivoInicial[2] = Venta.barrilActivoActual[2];
                    Venta.numLitrosBarrilActivoInicial[2] = Venta.numLitrosBarrilActivoActual[2];
                }
                else
                {
                    Venta.Consumo.VentaParcialGrifo3 += (Venta.numLitrosBarrilActivoInicial[2] -
                                                         Venta.numLitrosBarrilActivoActual[2]);
                    Venta.numLitrosBarrilActivoInicial[2] = Venta.numLitrosBarrilActivoActual[2];
                }
                #endregion

                #region TODOS LOS GRIFOS
                Venta.Consumo.VentaParcial = Venta.Consumo.VentaParcialGrifo1 +
                                             Venta.Consumo.VentaParcialGrifo2 +
                                             Venta.Consumo.VentaParcialGrifo3; 
                #endregion

                f.dibujaConsumo2(Venta.Consumo.VentaParcialGrifo1,
                                 Venta.Consumo.VentaParcialGrifo2,
                                 Venta.Consumo.VentaParcialGrifo3);
                #endregion
                #endregion


                #region ACTUALIZA EL ARRAY DE BYTES CON VALORES DE LA LECTURA
                //Actualiza celdas de memoria de ensamblado
                ensamblado.ActualizaValores(memoriaLogo);
                //Copia toda la memoria de logo en ensamblado
                ensamblado.ActualizaMemoria(memoriaLogo);
                #endregion


                #region COMPRUEBA SI EXISTE UN REGISTRO DE CONSUMO EN EL DÍA ACTUAL  Y LO GRABA
                #region OBTIENE LA FECHA ACTUAL EN EL FORMATO ADECUADO
                fechaActual = DateTime.Today;
                _fecha = (fechaActual.Year * 10000) +
                             (fechaActual.Month * 100) +
                             (fechaActual.Day);
                #endregion
                #region SI HA CAMBIADO LA FECHA O EL REGISTRO DEL DÍA NO EXISTE...
                if (conectadoBD)
                {
                    if ((_fecha > fecha) || !existeregistro)
                    {
                        #region SI NO EXISTE REGISTRO DE ESE DÍA...
                        if (!sql.comprobarConsumo(_fecha))
                        {
                            Venta.Consumo.Fecha = _fecha;
                            sql.grabarConsumo(Venta.Consumo);
                            if (ConectadoBD)
                                Log.LogInfo(DateTime.Now + " SE HA REGISTRADO EN BD EL CONSUMO DIARIO: ");
                            f.escribirDisplay(DateTime.Now + " SE HA REGISTRADO EN BD EL CONSUMO DIARIO: ");
                            existeregistro = true;
                            fecha = _fecha;
                            Venta.Consumo.VentaParcial = 0;
                            Venta.Consumo.VentaParcialGrifo1 = 0;
                            Venta.Consumo.VentaParcialGrifo2 = 0;
                            Venta.Consumo.VentaParcialGrifo3 = 0;
                        }
                        #endregion
                    }
                }
                #endregion
                #endregion

                /*DUERME EL HILO X MILÉSIMAS DE SEGUNDO*/
                Thread.Sleep(1000);
                #endregion

                #region SI SE PIERDE CONEXION CON LOGO INTENTA CONECTAR               
                if (ConectadoLogo && Resultado != 0)
                {
                    f.IndicadorSeñalOff();
                    f.escribirDisplay(DateTime.Now + " LOGO DESCONECTADO...");
                    do
                    {
                        try
                        {
                            Resultado = proceso.connectPlcIP();
                            f.escribirDisplay(DateTime.Now + " INTENTANDO CONECTAR CON LOGO...");
                        }
                        catch (Exception) { }
                    } while (ConectadoLogo && Resultado != 0);
                    if (ConectadoLogo && proceso.EstaConectado() && Resultado == 0)
                    {
                        try
                        {
                            f.IndicadorSeñalOn();
                            f.escribirDisplay(DateTime.Now + " CONEXION RESTABLECIDA!");
                            Resultado = 0;
                        }
                        catch (Exception) { }
                    }
                }
                #endregion 
                #endregion
            }
            #endregion
            //------------------------------------------------


            //------------------------------------------------
            #region OPERACIONES POSTERIORES AL BUCLE DEL HILO - FIN DEL HILO
            #region GRABA LOG
            if (ConectadoBD)
                Log.LogInfo(DateTime.Now + " SE HA DETENIDO EL SUBPROCESO ACTUAL CON EL LOGO");
            f.escribirDisplay(DateTime.Now + " SE HA DETENIDO EL SUBPROCESO ACTUAL CON EL LOGO");
            #endregion
            #region DETIENE EL HILO
            /*Detiene y elimina el hilo*/
            hilo.Abort();
            #endregion
            #endregion
            //------------------------------------------------
        }
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region IMPLEMENTACION DE INTERFACE IContract
        /// <summary>
        /// IMPLEMENTACION DE METODO EstablecerNumeroGrifos DE LA INTERFACE IContract
        /// </summary>
        /// <param name="texto"></param>
        public void EstablecerNumeroGrifos(int ngrifos)
        {
            NGrifos = ngrifos;
        }

        /// <summary>
        /// IMPLEMENTACION DE METODO CambiarEstadoConexion DE LA INTERFACE IContract
        /// </summary>
        /// <param name="texto"></param>
        public void CambiarEstadoConexion(bool estado)
        {
            ConectadoLogo = estado;
        }
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region Métodos generales     
        /// <summary>
        /// Obtiene los litros del barril indicado
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        private int ObtenerLitros(int i)
        {
            int valor = 0;
            switch (barrilActivo[i])
            {
                case 1:
                    valor = (memoriaLogo[3 + (i * 28)]) |
                                 (memoriaLogo[2 + (i * 28)] << 8) |
                                 (memoriaLogo[1 + (i * 28)] << 16) |
                                 (memoriaLogo[0 + (i * 28)] << 24);
                    break;
                case 2:
                    valor = (memoriaLogo[7 + (i * 28)]) |
                                 (memoriaLogo[6 + (i * 28)] << 8) |
                                 (memoriaLogo[5 + (i * 28)] << 16) |
                                 (memoriaLogo[4 + (i * 28)] << 24);
                    break;
                case 3:
                    valor = (memoriaLogo[11 + (i * 28)]) |
                                 (memoriaLogo[10 + (i * 28)] << 8) |
                                 (memoriaLogo[9 + (i * 28)] << 16) |
                                 (memoriaLogo[8 + (i * 28)] << 24);
                    break;
                default:
                    valor = 0;
                    break;
            }
            return valor;
        }

        /// <summary>
        /// Aborta el hilo
        /// </summary>
        public void AbortaHilo()
        {
            hilo.Abort();
        }
        #endregion
        /*################################################################*/


        /*################################################################*/
        #region Propiedades
        /// <summary>
        /// Comprueba conexión con logo
        /// </summary>
        public bool ConectadoLogo { get => conectadoLogo; set => conectadoLogo = value; }

        /// <summary>
        /// Comprueba conexión con BBDD
        /// </summary>
        public bool ConectadoBD { get => conectadoBD; set => conectadoBD = value; }

        /// <summary>
        /// Obtiene el número de grifos del sistema actual
        /// </summary>
        public int NGrifos
        {
            get { return nGrifos; }
            set { nGrifos = value; }
        }

        /// <summary>
        /// Obtiene el número de barriles VentaParcials
        /// </summary>
        public Ventas Venta
        {
            get { return ventas; }
            set { ventas = value; }
        }
        #endregion
        /*################################################################*/
    }
}
