﻿namespace logo2020.PRESENTATION
{
    partial class FormEstablecerGrifos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cerrarEstablecerGrifos = new System.Windows.Forms.Button();
            this.grifos_enviar = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.comboGrifos = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cerrarEstablecerGrifos
            // 
            this.cerrarEstablecerGrifos.FlatAppearance.BorderSize = 0;
            this.cerrarEstablecerGrifos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cerrarEstablecerGrifos.Image = global::logo2020.PRESENTATION.Properties.Resources.CERRAR;
            this.cerrarEstablecerGrifos.Location = new System.Drawing.Point(16, 140);
            this.cerrarEstablecerGrifos.Name = "cerrarEstablecerGrifos";
            this.cerrarEstablecerGrifos.Size = new System.Drawing.Size(75, 40);
            this.cerrarEstablecerGrifos.TabIndex = 40;
            this.cerrarEstablecerGrifos.UseVisualStyleBackColor = true;
            this.cerrarEstablecerGrifos.Visible = false;
            this.cerrarEstablecerGrifos.Click += new System.EventHandler(this.cerrarEstablecerGrifos_Click);
            // 
            // grifos_enviar
            // 
            this.grifos_enviar.FlatAppearance.BorderSize = 0;
            this.grifos_enviar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grifos_enviar.Image = global::logo2020.PRESENTATION.Properties.Resources.enviar;
            this.grifos_enviar.Location = new System.Drawing.Point(322, 135);
            this.grifos_enviar.Name = "grifos_enviar";
            this.grifos_enviar.Size = new System.Drawing.Size(75, 50);
            this.grifos_enviar.TabIndex = 39;
            this.grifos_enviar.UseVisualStyleBackColor = true;
            this.grifos_enviar.Click += new System.EventHandler(this.grifos_enviar_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label56);
            this.panel1.Controls.Add(this.comboGrifos);
            this.panel1.Controls.Add(this.cerrarEstablecerGrifos);
            this.panel1.Controls.Add(this.grifos_enviar);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(416, 190);
            this.panel1.TabIndex = 41;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(384, 36);
            this.label1.TabIndex = 43;
            this.label1.Text = "POR FAVOR,\r\nINDICA EL NÚMERO DE GRIFOS QUE TIENE EL SISTEMA";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(13, 75);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(136, 18);
            this.label56.TabIndex = 42;
            this.label56.Text = "NÚMERO DE LINEAS";
            // 
            // comboGrifos
            // 
            this.comboGrifos.AllowDrop = true;
            this.comboGrifos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboGrifos.FormattingEnabled = true;
            this.comboGrifos.Items.AddRange(new object[] {
            "UN GRIFO",
            "DOS GRIFOS",
            "TRES GRIFOS"});
            this.comboGrifos.Location = new System.Drawing.Point(16, 96);
            this.comboGrifos.Name = "comboGrifos";
            this.comboGrifos.Size = new System.Drawing.Size(224, 21);
            this.comboGrifos.TabIndex = 41;
            // 
            // FormEstablecerGrifos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 214);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormEstablecerGrifos";
            this.Text = "ESTABLECE EL NUMERO DE GRIFOS";
            this.Load += new System.EventHandler(this.FormEstablecerGrifos_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button cerrarEstablecerGrifos;
        private System.Windows.Forms.Button grifos_enviar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.ComboBox comboGrifos;
        private System.Windows.Forms.Label label1;
    }
}