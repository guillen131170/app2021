﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logo2020.IFR
{
    #region INTERFACE IContract PARA PASO DE VARIABLES ENTRE FORMULARIOS
    public interface IContract
    {
        /// <summary>
        /// IMPLEMENTACION DEL METODO EstablecerNumeroGrifos DE LA INTERFACE IContract
        /// </summary>
        /// <param name="ngrifos"></param>
        void EstablecerNumeroGrifos(int ngrifos);

        /// <summary>
        /// IMPLEMENTACION DEL METODO CambiarEstadoConexion DE LA INTERFACE IContract
        /// </summary>
        /// <param name="estado"></param>
        void CambiarEstadoConexion(bool estado);

        /// <summary>
        /// IMPLEMENTACION DEL METODO CambiarEstadoAlarmas DE LA INTERFACE IContract
        /// </summary>
        /// <param name="estado"></param>
        void CambiarEstadoAlarmas(bool a, bool b, bool c, bool d);
    } 
    #endregion
}
